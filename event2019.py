# -*- coding: utf-8 -*-

import math
from collections import namedtuple, deque, Counter
from itertools import permutations
from typing import List, Dict, Deque
import asyncio

from defaultlist import defaultlist


def day1_part1(in_: str) -> None:
    print(sum([int(line) // 3 - 2 for line in in_.splitlines()]))


def day1_part2(in_: str) -> None:
    def needed_fuel(mass: int) -> int:
        current_mass = mass
        total_fuel = 0

        while current_mass > 0:
            current_mass = current_mass // 3 - 2
            if current_mass > 0:
                total_fuel += current_mass

        return total_fuel

    print(sum([needed_fuel(int(line)) for line in in_.splitlines()]))


def day2_part1(in_: str) -> None:
    intcode = [int(i) for i in in_.split(',')]
    intcode[1] = 12
    intcode[2] = 2

    ip = 0
    while intcode[ip] != 99:
        if intcode[ip] == 1:
            intcode[intcode[ip + 3]] = intcode[intcode[ip + 1]] + intcode[intcode[ip + 2]]
        elif intcode[ip] == 2:
            intcode[intcode[ip + 3]] = intcode[intcode[ip + 1]] * intcode[intcode[ip + 2]]
        else:
            assert False
        ip += 4

    print(intcode[0])


def day2_part2(in_: str) -> None:
    BASE_MEMORY = [int(i) for i in in_.split(',')]
    GOAL = 19690720

    for noun in range(100):
        for verb in range(100):
            memory = BASE_MEMORY.copy()
            memory[1] = noun
            memory[2] = verb
            ip = 0

            while memory[ip] != 99:
                if memory[ip] == 1:
                    memory[memory[ip + 3]] = memory[memory[ip + 1]] + memory[memory[ip + 2]]
                elif memory[ip] == 2:
                    memory[memory[ip + 3]] = memory[memory[ip + 1]] * memory[memory[ip + 2]]
                else:
                    assert False
                ip += 4

            if memory[0] == GOAL:
                print(100 * noun + verb)
                return

    assert False


def day3_part1(in_: str) -> None:
    wires_paths = [line.split(',') for line in in_.splitlines()]
    wires_used_cells = [set(), set()]

    for wire_path, wire_used_cell in zip(wires_paths, wires_used_cells):
        cur_pos = (0, 0)
        for section in wire_path:
            dist = int(section[1:])
            if section[0] == 'L':
                for i in range(1, dist + 1):
                    wire_used_cell.add((cur_pos[0] - i, cur_pos[1]))
                cur_pos = (cur_pos[0] - dist, cur_pos[1])
            elif section[0] == 'R':
                for i in range(1, dist + 1):
                    wire_used_cell.add((cur_pos[0] + i, cur_pos[1]))
                cur_pos = (cur_pos[0] + dist, cur_pos[1])
            elif section[0] == 'U':
                for i in range(1, dist + 1):
                    wire_used_cell.add((cur_pos[0], cur_pos[1] - i))
                cur_pos = (cur_pos[0], cur_pos[1] - dist)
            elif section[0] == 'D':
                for i in range(1, dist + 1):
                    wire_used_cell.add((cur_pos[0], cur_pos[1] + i))
                cur_pos = (cur_pos[0], cur_pos[1] + dist)
            else:
                assert False

    nearest = math.inf
    for inters in wires_used_cells[0].intersection(wires_used_cells[1]):
        if abs(inters[0]) + abs(inters[1]) < nearest:
            nearest = abs(inters[0]) + abs(inters[1])
    print(nearest)


def day3_part2(in_: str) -> None:
    wires_paths = [line.split(',') for line in in_.splitlines()]
    wires_used_cells = [set(), set()]
    wires_cells_steps = [{}, {}]

    for wire_path, wire_used_cells, wire_cells_steps in zip(wires_paths, wires_used_cells, wires_cells_steps):
        cur_pos = (0, 0)
        step = 0
        for section in wire_path:
            dist = int(section[1:])
            if section[0] == 'L':
                for i in range(1, dist + 1):
                    cell = (cur_pos[0] - i, cur_pos[1])
                    wire_used_cells.add(cell)
                    if cell not in wire_cells_steps:
                        wire_cells_steps[cell] = step + i
                cur_pos = (cur_pos[0] - dist, cur_pos[1])
            elif section[0] == 'R':
                for i in range(1, dist + 1):
                    cell = (cur_pos[0] + i, cur_pos[1])
                    wire_used_cells.add(cell)
                    if cell not in wire_cells_steps:
                        wire_cells_steps[cell] = step + i
                cur_pos = (cur_pos[0] + dist, cur_pos[1])
            elif section[0] == 'U':
                for i in range(1, dist + 1):
                    cell = (cur_pos[0], cur_pos[1] - i)
                    wire_used_cells.add(cell)
                    if cell not in wire_cells_steps:
                        wire_cells_steps[cell] = step + i
                cur_pos = (cur_pos[0], cur_pos[1] - dist)
            elif section[0] == 'D':
                for i in range(1, dist + 1):
                    cell = (cur_pos[0], cur_pos[1] + i)
                    wire_used_cells.add(cell)
                    if cell not in wire_cells_steps:
                        wire_cells_steps[cell] = step + i
                cur_pos = (cur_pos[0], cur_pos[1] + dist)
            else:
                assert False
            step += dist

    nearest = math.inf
    for inters in wires_used_cells[0].intersection(wires_used_cells[1]):
        if wires_cells_steps[0][inters] + wires_cells_steps[1][inters] < nearest:
            nearest = wires_cells_steps[0][inters] + wires_cells_steps[1][inters]
    print(nearest)


def day4_part1(in_: str) -> None:
    in_range = [int(n) for n in in_.split('-')]
    n_works = 0

    for i in range(in_range[0], in_range[1] + 1):
        str_i = str(i)
        has_double = False
        for j in range(5):
            if str_i[j] == str_i[j + 1]:
                has_double = True
            if int(str_i[j]) > int(str_i[j + 1]):
                break
        else:
            if has_double:
                n_works += 1

    print(n_works)


def day4_part2(in_: str) -> None:
    in_range = [int(n) for n in in_.split('-')]
    n_works = 0

    for i in range(in_range[0], in_range[1] + 1):
        str_i = str(i)
        has_double = False
        for j in range(5):
            if str_i[j] == str_i[j + 1]\
                    and (j == 0 or str_i[j] != str_i[j - 1])\
                    and (j == len(str_i) - 2 or str_i[j] != str_i[j + 2]):
                has_double = True
            if int(str_i[j]) > int(str_i[j + 1]):
                break
        else:
            if has_double:
                n_works += 1

    print(n_works)


def day5_part1(in_: str) -> None:
    memory = [int(i) for i in in_.split(',')]
    ip = 0
    globalInput = 1

    def get_value(position: int, mode: int):
        if mode == 0:
            return memory[memory[position]]
        elif mode == 1:
            return memory[position]
        else:
            assert False

    def set_value(position: int, value: int):
        memory[memory[position]] = value

    InstructionDefinition = namedtuple('InstructionDefinition', ['number_of_parameters', 'callable'])

    def instruction_add(modes: List[int]) -> True:
        firstValue = get_value(ip + 1, modes[0])
        secondValue = get_value(ip + 2, modes[1])
        assert modes[2] == 0
        set_value(ip + 3, firstValue + secondValue)
        return True

    def instruction_multiply(modes: List[int]) -> True:
        firstValue = get_value(ip + 1, modes[0])
        secondValue = get_value(ip + 2, modes[1])
        assert modes[2] == 0
        set_value(ip + 3, firstValue * secondValue)
        return True

    def instruction_read(modes: List[int]) -> True:
        assert modes[0] == 0
        set_value(ip + 1, globalInput)
        return True

    def instruction_write(modes: List[int]) -> True:
        value = get_value(ip + 1, modes[0])
        print(value)
        return True

    def instruction_exit(_) -> False:
        return False

    INSTRUCTIONS = {
        1: InstructionDefinition(3, instruction_add),
        2: InstructionDefinition(3, instruction_multiply),
        3: InstructionDefinition(1, instruction_read),
        4: InstructionDefinition(1, instruction_write),
        99: InstructionDefinition(0, instruction_exit)
    }

    continue_execution = True
    while continue_execution:
        head = str(memory[ip]).zfill(5)
        opcode = int(head[3:])
        modes = [int(char) for char in head[:3][::-1]]
        continue_execution = INSTRUCTIONS[opcode].callable(modes)
        ip += INSTRUCTIONS[opcode].number_of_parameters + 1


def day5_part2(in_: str) -> None:
    memory = [int(i) for i in in_.split(',')]
    ip = 0
    globalInput = 5

    def get_value(position: int, mode: int):
        if mode == 0:
            return memory[memory[position]]
        elif mode == 1:
            return memory[position]
        else:
            assert False

    def set_value(position: int, value: int):
        memory[memory[position]] = value

    InstructionDefinition = namedtuple('InstructionDefinition', ['number_of_parameters', 'callable'])

    def instruction_add(modes: List[int]) -> Dict:
        firstValue = get_value(ip + 1, modes[0])
        secondValue = get_value(ip + 2, modes[1])
        assert modes[2] == 0
        set_value(ip + 3, firstValue + secondValue)
        return {"continue_execution": True, "increment_ip": True}

    def instruction_multiply(modes: List[int]) -> Dict:
        firstValue = get_value(ip + 1, modes[0])
        secondValue = get_value(ip + 2, modes[1])
        assert modes[2] == 0
        set_value(ip + 3, firstValue * secondValue)
        return {"continue_execution": True, "increment_ip": True}

    def instruction_read(modes: List[int]) -> Dict:
        assert modes[0] == 0
        set_value(ip + 1, globalInput)
        return {"continue_execution": True, "increment_ip": True}

    def instruction_write(modes: List[int]) -> Dict:
        value = get_value(ip + 1, modes[0])
        print(value)
        return {"continue_execution": True, "increment_ip": True}

    def instruction_jumpiftrue(modes: List[int]) -> Dict:
        nonlocal ip
        firstValue = get_value(ip + 1, modes[0])
        secondValue = get_value(ip + 2, modes[1])
        if firstValue != 0:
            ip = secondValue
            return {"continue_execution": True, "increment_ip": False}
        return {"continue_execution": True, "increment_ip": True}

    def instruction_jumpiffalse(modes: List[int]) -> Dict:
        nonlocal ip
        firstValue = get_value(ip + 1, modes[0])
        secondValue = get_value(ip + 2, modes[1])
        if firstValue == 0:
            ip = secondValue
            return {"continue_execution": True, "increment_ip": False}
        return {"continue_execution": True, "increment_ip": True}

    def instruction_lessthan(modes: List[int]) -> Dict:
        firstValue = get_value(ip + 1, modes[0])
        secondValue = get_value(ip + 2, modes[1])
        set_value(ip + 3, 1 if firstValue < secondValue else 0)
        return {"continue_execution": True, "increment_ip": True}

    def instruction_equals(modes: List[int]) -> Dict:
        firstValue = get_value(ip + 1, modes[0])
        secondValue = get_value(ip + 2, modes[1])
        set_value(ip + 3, 1 if firstValue == secondValue else 0)
        return {"continue_execution": True, "increment_ip": True}

    def instruction_exit(_) -> Dict:
        return {"continue_execution": False, "increment_ip": False}

    INSTRUCTIONS = {
        1: InstructionDefinition(3, instruction_add),
        2: InstructionDefinition(3, instruction_multiply),
        3: InstructionDefinition(1, instruction_read),
        4: InstructionDefinition(1, instruction_write),
        5: InstructionDefinition(2, instruction_jumpiftrue),
        6: InstructionDefinition(2, instruction_jumpiffalse),
        7: InstructionDefinition(3, instruction_lessthan),
        8: InstructionDefinition(3, instruction_equals),
        99: InstructionDefinition(0, instruction_exit)
    }

    continue_execution = True
    while continue_execution:
        head = str(memory[ip]).zfill(5)
        opcode = int(head[3:])
        modes = [int(char) for char in head[:3][::-1]]

        output = INSTRUCTIONS[opcode].callable(modes)
        if output['increment_ip']:
            ip += INSTRUCTIONS[opcode].number_of_parameters + 1
        continue_execution = output['continue_execution']


def day6_part1(in_: str) -> None:
    orbits_directly = {}
    for line in in_.splitlines():
        left, right = line.split(')')
        orbits_directly[right] = left

    num_orbits = 0
    for obj in orbits_directly.keys():
        tmp = obj
        while tmp != 'COM':
            num_orbits += 1
            tmp = orbits_directly[tmp]
    print(num_orbits)


def day6_part2(in_: str) -> None:
    orbits_directly = {}
    for line in in_.splitlines():
        left, right = line.split(')')
        try:
            orbits_directly[left].append(right)
        except KeyError:
            orbits_directly[left] = [right]
        try:
            orbits_directly[right].append(left)
        except KeyError:
            orbits_directly[right] = [left]

    def find_shortest_path(graph, start, end, path=None):
        if path is None:
            path = []
        path = path + [start]

        if start == end:
            return path
        if start not in graph:
            return None

        shortest = None
        for node in graph[start]:
            if node not in path:
                newpath = find_shortest_path(graph, node, end, path)
                if newpath:
                    if not shortest or len(newpath) < len(shortest):
                        shortest = newpath
        return shortest

    print(len(find_shortest_path(orbits_directly, orbits_directly['YOU'][0], orbits_directly['SAN'][0])) - 1)


def day7_part1(in_: str) -> None:
    def execute_intcode(intcode: List[int], input_list: Deque[int], output_list: Deque[int]) -> None:
        memory = intcode.copy()
        ip = 0

        def get_value(position: int, mode: int):
            if mode == 0:
                return memory[memory[position]]
            elif mode == 1:
                return memory[position]
            else:
                assert False

        def set_value(position: int, value: int):
            memory[memory[position]] = value

        InstructionDefinition = namedtuple('InstructionDefinition', ['number_of_parameters', 'callable'])

        def instruction_add(modes: List[int]) -> Dict:
            firstValue = get_value(ip + 1, modes[0])
            secondValue = get_value(ip + 2, modes[1])
            assert modes[2] == 0
            set_value(ip + 3, firstValue + secondValue)
            return {"continue_execution": True, "increment_ip": True}

        def instruction_multiply(modes: List[int]) -> Dict:
            firstValue = get_value(ip + 1, modes[0])
            secondValue = get_value(ip + 2, modes[1])
            assert modes[2] == 0
            set_value(ip + 3, firstValue * secondValue)
            return {"continue_execution": True, "increment_ip": True}

        def instruction_read(modes: List[int]) -> Dict:
            assert modes[0] == 0
            set_value(ip + 1, input_list.popleft())
            return {"continue_execution": True, "increment_ip": True}

        def instruction_write(modes: List[int]) -> Dict:
            value = get_value(ip + 1, modes[0])
            output_list.append(value)
            return {"continue_execution": True, "increment_ip": True}

        def instruction_jumpiftrue(modes: List[int]) -> Dict:
            nonlocal ip
            firstValue = get_value(ip + 1, modes[0])
            secondValue = get_value(ip + 2, modes[1])
            if firstValue != 0:
                ip = secondValue
                return {"continue_execution": True, "increment_ip": False}
            return {"continue_execution": True, "increment_ip": True}

        def instruction_jumpiffalse(modes: List[int]) -> Dict:
            nonlocal ip
            firstValue = get_value(ip + 1, modes[0])
            secondValue = get_value(ip + 2, modes[1])
            if firstValue == 0:
                ip = secondValue
                return {"continue_execution": True, "increment_ip": False}
            return {"continue_execution": True, "increment_ip": True}

        def instruction_lessthan(modes: List[int]) -> Dict:
            firstValue = get_value(ip + 1, modes[0])
            secondValue = get_value(ip + 2, modes[1])
            set_value(ip + 3, 1 if firstValue < secondValue else 0)
            return {"continue_execution": True, "increment_ip": True}

        def instruction_equals(modes: List[int]) -> Dict:
            firstValue = get_value(ip + 1, modes[0])
            secondValue = get_value(ip + 2, modes[1])
            set_value(ip + 3, 1 if firstValue == secondValue else 0)
            return {"continue_execution": True, "increment_ip": True}

        def instruction_exit(_) -> Dict:
            return {"continue_execution": False, "increment_ip": False}

        INSTRUCTIONS = {
            1: InstructionDefinition(3, instruction_add),
            2: InstructionDefinition(3, instruction_multiply),
            3: InstructionDefinition(1, instruction_read),
            4: InstructionDefinition(1, instruction_write),
            5: InstructionDefinition(2, instruction_jumpiftrue),
            6: InstructionDefinition(2, instruction_jumpiffalse),
            7: InstructionDefinition(3, instruction_lessthan),
            8: InstructionDefinition(3, instruction_equals),
            99: InstructionDefinition(0, instruction_exit)
        }

        continue_execution = True
        while continue_execution:
            head = str(memory[ip]).zfill(5)
            opcode = int(head[3:])
            modes = [int(char) for char in head[:3][::-1]]

            output = INSTRUCTIONS[opcode].callable(modes)
            if output['increment_ip']:
                ip += INSTRUCTIONS[opcode].number_of_parameters + 1
            continue_execution = output['continue_execution']

    intcode = [int(i) for i in in_.split(',')]
    max_signal = 0
    for phase_setting in permutations(range(5), 5):
        signal = 0
        for i in range(len(phase_setting)):
            input_list = deque([phase_setting[i], signal])
            output_list = deque()
            execute_intcode(intcode, input_list, output_list)
            signal = output_list.popleft()
            max_signal = max(max_signal, signal)

    print(max_signal)


async def day7_part2(in_: str) -> None:
    async def execute_intcode(intcode: List[int], input_queue: asyncio.Queue, output_queue: asyncio.Queue) -> None:
        memory = intcode.copy()
        ip = 0

        async def get_value(position: int, mode: int):
            if mode == 0:
                return memory[memory[position]]
            elif mode == 1:
                return memory[position]
            else:
                assert False

        async def set_value(position: int, value: int):
            memory[memory[position]] = value

        InstructionDefinition = namedtuple('InstructionDefinition', ['number_of_parameters', 'callable'])

        async def instruction_add(modes: List[int]) -> Dict:
            firstValue = await get_value(ip + 1, modes[0])
            secondValue = await get_value(ip + 2, modes[1])
            assert modes[2] == 0
            await set_value(ip + 3, firstValue + secondValue)
            return {"continue_execution": True, "increment_ip": True}

        async def instruction_multiply(modes: List[int]) -> Dict:
            firstValue = await get_value(ip + 1, modes[0])
            secondValue = await get_value(ip + 2, modes[1])
            assert modes[2] == 0
            await set_value(ip + 3, firstValue * secondValue)
            return {"continue_execution": True, "increment_ip": True}

        async def instruction_read(modes: List[int]) -> Dict:
            assert modes[0] == 0
            await set_value(ip + 1, await input_queue.get())
            return {"continue_execution": True, "increment_ip": True}

        async def instruction_write(modes: List[int]) -> Dict:
            value = await get_value(ip + 1, modes[0])
            await output_queue.put(value)
            return {"continue_execution": True, "increment_ip": True}

        async def instruction_jumpiftrue(modes: List[int]) -> Dict:
            nonlocal ip
            firstValue = await get_value(ip + 1, modes[0])
            secondValue = await get_value(ip + 2, modes[1])
            if firstValue != 0:
                ip = secondValue
                return {"continue_execution": True, "increment_ip": False}
            return {"continue_execution": True, "increment_ip": True}

        async def instruction_jumpiffalse(modes: List[int]) -> Dict:
            nonlocal ip
            firstValue = await get_value(ip + 1, modes[0])
            secondValue = await get_value(ip + 2, modes[1])
            if firstValue == 0:
                ip = secondValue
                return {"continue_execution": True, "increment_ip": False}
            return {"continue_execution": True, "increment_ip": True}

        async def instruction_lessthan(modes: List[int]) -> Dict:
            firstValue = await get_value(ip + 1, modes[0])
            secondValue = await get_value(ip + 2, modes[1])
            set_value(ip + 3, 1 if firstValue < secondValue else 0)
            return {"continue_execution": True, "increment_ip": True}

        async def instruction_equals(modes: List[int]) -> Dict:
            firstValue = await get_value(ip + 1, modes[0])
            secondValue = await get_value(ip + 2, modes[1])
            set_value(ip + 3, 1 if firstValue == secondValue else 0)
            return {"continue_execution": True, "increment_ip": True}

        async def instruction_exit(_) -> Dict:
            return {"continue_execution": False, "increment_ip": False}

        INSTRUCTIONS = {
            1: InstructionDefinition(3, instruction_add),
            2: InstructionDefinition(3, instruction_multiply),
            3: InstructionDefinition(1, instruction_read),
            4: InstructionDefinition(1, instruction_write),
            5: InstructionDefinition(2, instruction_jumpiftrue),
            6: InstructionDefinition(2, instruction_jumpiffalse),
            7: InstructionDefinition(3, instruction_lessthan),
            8: InstructionDefinition(3, instruction_equals),
            99: InstructionDefinition(0, instruction_exit)
        }

        continue_execution = True
        while continue_execution:
            head = str(memory[ip]).zfill(5)
            opcode = int(head[3:])
            modes = [int(char) for char in head[:3][::-1]]

            output = await INSTRUCTIONS[opcode].callable(modes)
            if output['increment_ip']:
                ip += INSTRUCTIONS[opcode].number_of_parameters + 1
            continue_execution = output['continue_execution']

    intcode = [int(i) for i in in_.split(',')]
    max_signal = 0
    for phase_setting in permutations(range(5, 10), 5):
        queue_e_a = asyncio.Queue()
        await queue_e_a.put(phase_setting[0])
        queue_a_b = asyncio.Queue()
        await queue_a_b.put(phase_setting[1])
        queue_b_c = asyncio.Queue()
        await queue_b_c.put(phase_setting[2])
        queue_c_d = asyncio.Queue()
        await queue_c_d.put(phase_setting[3])
        queue_d_e = asyncio.Queue()
        await queue_d_e.put(phase_setting[4])
        await queue_e_a.put(0)

        asyncio.create_task(execute_intcode(intcode, queue_e_a, queue_a_b))
        asyncio.create_task(execute_intcode(intcode, queue_a_b, queue_b_c))
        asyncio.create_task(execute_intcode(intcode, queue_b_c, queue_c_d))
        asyncio.create_task(execute_intcode(intcode, queue_c_d, queue_d_e))
        await asyncio.create_task(execute_intcode(intcode, queue_d_e, queue_e_a))

        max_signal = max(max_signal, await queue_e_a.get())

    print(max_signal)


def day8_part1(in_: str) -> None:
    digits = [int(c) for c in in_.strip()]
    layers = [digits[i * 25 * 6:(i + 1) * 25 * 6] for i in range(len(digits) // (25 * 6))]
    counters = [Counter(layer) for layer in layers]
    counter_min_zero = min(counters, key=lambda x: x[0])
    print(counter_min_zero[1] * counter_min_zero[2])


def day8_part2(in_: str) -> None:
    digits = in_.strip()
    layers = [digits[i * 25 * 6:(i + 1) * 25 * 6] for i in range(len(digits) // (25 * 6))]

    image = list(layers[0])
    for i in range(len(image)):
        j = 1
        while image[i] == '2':
            image[i] = layers[j][i]
            j += 1

    for r in range(6):
        print(''.join(image[r * 25:(r + 1) * 25]).replace('0', ' ').replace('1', 'X'))


def day9_part1(in_: str) -> None:
    def execute_intcode(intcode: List[int], input_list: Deque[int], output_list: Deque[int]) -> None:
        memory = defaultlist(lambda: 0) + intcode.copy()
        ip = 0
        relative_base = 0

        def get_value(position: int, mode: int):
            if mode == 0:
                return memory[memory[position]]
            elif mode == 1:
                return memory[position]
            elif mode == 2:
                return memory[relative_base + memory[position]]
            else:
                assert False

        def set_value(position: int, mode: int, value: int):
            if mode == 0:
                memory[memory[position]] = value
            elif mode == 2:
                memory[relative_base + memory[position]] = value
            else:
                assert False

        InstructionDefinition = namedtuple('InstructionDefinition', ['number_of_parameters', 'callable'])

        def instruction_add(modes: List[int]) -> Dict:
            firstValue = get_value(ip + 1, modes[0])
            secondValue = get_value(ip + 2, modes[1])
            set_value(ip + 3, modes[2], firstValue + secondValue)
            return {"continue_execution": True, "increment_ip": True}

        def instruction_multiply(modes: List[int]) -> Dict:
            firstValue = get_value(ip + 1, modes[0])
            secondValue = get_value(ip + 2, modes[1])
            set_value(ip + 3, modes[2], firstValue * secondValue)
            return {"continue_execution": True, "increment_ip": True}

        def instruction_read(modes: List[int]) -> Dict:
            set_value(ip + 1, modes[0], input_list.popleft())
            return {"continue_execution": True, "increment_ip": True}

        def instruction_write(modes: List[int]) -> Dict:
            value = get_value(ip + 1, modes[0])
            output_list.append(value)
            return {"continue_execution": True, "increment_ip": True}

        def instruction_jumpiftrue(modes: List[int]) -> Dict:
            nonlocal ip
            firstValue = get_value(ip + 1, modes[0])
            secondValue = get_value(ip + 2, modes[1])
            if firstValue != 0:
                ip = secondValue
                return {"continue_execution": True, "increment_ip": False}
            return {"continue_execution": True, "increment_ip": True}

        def instruction_jumpiffalse(modes: List[int]) -> Dict:
            nonlocal ip
            firstValue = get_value(ip + 1, modes[0])
            secondValue = get_value(ip + 2, modes[1])
            if firstValue == 0:
                ip = secondValue
                return {"continue_execution": True, "increment_ip": False}
            return {"continue_execution": True, "increment_ip": True}

        def instruction_lessthan(modes: List[int]) -> Dict:
            firstValue = get_value(ip + 1, modes[0])
            secondValue = get_value(ip + 2, modes[1])
            set_value(ip + 3, modes[2], 1 if firstValue < secondValue else 0)
            return {"continue_execution": True, "increment_ip": True}

        def instruction_equals(modes: List[int]) -> Dict:
            firstValue = get_value(ip + 1, modes[0])
            secondValue = get_value(ip + 2, modes[1])
            set_value(ip + 3, modes[2], 1 if firstValue == secondValue else 0)
            return {"continue_execution": True, "increment_ip": True}

        def instruction_relativebase(modes: List[int]) -> Dict:
            nonlocal relative_base
            value = get_value(ip + 1, modes[0])
            relative_base += value
            return {"continue_execution": True, "increment_ip": True}

        def instruction_exit(_) -> Dict:
            return {"continue_execution": False, "increment_ip": False}

        INSTRUCTIONS = {
            1: InstructionDefinition(3, instruction_add),
            2: InstructionDefinition(3, instruction_multiply),
            3: InstructionDefinition(1, instruction_read),
            4: InstructionDefinition(1, instruction_write),
            5: InstructionDefinition(2, instruction_jumpiftrue),
            6: InstructionDefinition(2, instruction_jumpiffalse),
            7: InstructionDefinition(3, instruction_lessthan),
            8: InstructionDefinition(3, instruction_equals),
            9: InstructionDefinition(1, instruction_relativebase),
            99: InstructionDefinition(0, instruction_exit)
        }

        continue_execution = True
        while continue_execution:
            head = str(memory[ip]).zfill(5)
            opcode = int(head[3:])
            modes = [int(char) for char in head[:3][::-1]]

            output = INSTRUCTIONS[opcode].callable(modes)
            if output['increment_ip']:
                ip += INSTRUCTIONS[opcode].number_of_parameters + 1
            continue_execution = output['continue_execution']

    intcode = [int(i) for i in in_.split(',')]
    input_list = deque([1])
    output_list = deque()
    execute_intcode(intcode, input_list, output_list)
    print(output_list.popleft())


def day9_part2(in_: str) -> None:
    def execute_intcode(intcode: List[int], input_list: Deque[int], output_list: Deque[int]) -> None:
        memory = defaultlist(lambda: 0) + intcode.copy()
        ip = 0
        relative_base = 0

        def get_value(position: int, mode: int):
            if mode == 0:
                return memory[memory[position]]
            elif mode == 1:
                return memory[position]
            elif mode == 2:
                return memory[relative_base + memory[position]]
            else:
                assert False

        def set_value(position: int, mode: int, value: int):
            if mode == 0:
                memory[memory[position]] = value
            elif mode == 2:
                memory[relative_base + memory[position]] = value
            else:
                assert False

        InstructionDefinition = namedtuple('InstructionDefinition', ['number_of_parameters', 'callable'])

        def instruction_add(modes: List[int]) -> Dict:
            firstValue = get_value(ip + 1, modes[0])
            secondValue = get_value(ip + 2, modes[1])
            set_value(ip + 3, modes[2], firstValue + secondValue)
            return {"continue_execution": True, "increment_ip": True}

        def instruction_multiply(modes: List[int]) -> Dict:
            firstValue = get_value(ip + 1, modes[0])
            secondValue = get_value(ip + 2, modes[1])
            set_value(ip + 3, modes[2], firstValue * secondValue)
            return {"continue_execution": True, "increment_ip": True}

        def instruction_read(modes: List[int]) -> Dict:
            set_value(ip + 1, modes[0], input_list.popleft())
            return {"continue_execution": True, "increment_ip": True}

        def instruction_write(modes: List[int]) -> Dict:
            value = get_value(ip + 1, modes[0])
            output_list.append(value)
            return {"continue_execution": True, "increment_ip": True}

        def instruction_jumpiftrue(modes: List[int]) -> Dict:
            nonlocal ip
            firstValue = get_value(ip + 1, modes[0])
            secondValue = get_value(ip + 2, modes[1])
            if firstValue != 0:
                ip = secondValue
                return {"continue_execution": True, "increment_ip": False}
            return {"continue_execution": True, "increment_ip": True}

        def instruction_jumpiffalse(modes: List[int]) -> Dict:
            nonlocal ip
            firstValue = get_value(ip + 1, modes[0])
            secondValue = get_value(ip + 2, modes[1])
            if firstValue == 0:
                ip = secondValue
                return {"continue_execution": True, "increment_ip": False}
            return {"continue_execution": True, "increment_ip": True}

        def instruction_lessthan(modes: List[int]) -> Dict:
            firstValue = get_value(ip + 1, modes[0])
            secondValue = get_value(ip + 2, modes[1])
            set_value(ip + 3, modes[2], 1 if firstValue < secondValue else 0)
            return {"continue_execution": True, "increment_ip": True}

        def instruction_equals(modes: List[int]) -> Dict:
            firstValue = get_value(ip + 1, modes[0])
            secondValue = get_value(ip + 2, modes[1])
            set_value(ip + 3, modes[2], 1 if firstValue == secondValue else 0)
            return {"continue_execution": True, "increment_ip": True}

        def instruction_relativebase(modes: List[int]) -> Dict:
            nonlocal relative_base
            value = get_value(ip + 1, modes[0])
            relative_base += value
            return {"continue_execution": True, "increment_ip": True}

        def instruction_exit(_) -> Dict:
            return {"continue_execution": False, "increment_ip": False}

        INSTRUCTIONS = {
            1: InstructionDefinition(3, instruction_add),
            2: InstructionDefinition(3, instruction_multiply),
            3: InstructionDefinition(1, instruction_read),
            4: InstructionDefinition(1, instruction_write),
            5: InstructionDefinition(2, instruction_jumpiftrue),
            6: InstructionDefinition(2, instruction_jumpiffalse),
            7: InstructionDefinition(3, instruction_lessthan),
            8: InstructionDefinition(3, instruction_equals),
            9: InstructionDefinition(1, instruction_relativebase),
            99: InstructionDefinition(0, instruction_exit)
        }

        continue_execution = True
        while continue_execution:
            head = str(memory[ip]).zfill(5)
            opcode = int(head[3:])
            modes = [int(char) for char in head[:3][::-1]]

            output = INSTRUCTIONS[opcode].callable(modes)
            if output['increment_ip']:
                ip += INSTRUCTIONS[opcode].number_of_parameters + 1
            continue_execution = output['continue_execution']

    intcode = [int(i) for i in in_.split(',')]
    input_list = deque([2])
    output_list = deque()
    execute_intcode(intcode, input_list, output_list)
    print(output_list.popleft())

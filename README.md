# Advent of Code 🐍

Resolution of https://adventofcode.com/ in Python

## Notes

Most of the time, the right answer is printed on the standard output.
Here is a list of the the exercises that works differently:
 - `2015.19.2`, `2015.22`: the execution is infinite and a better answer is printed each time one is found
 - `2016.8.2`, `2018.10.1`: a graphic representation is printed
 - `2019.5.1`: the program executes the input, which does commands to output different values but only the last one is the right one

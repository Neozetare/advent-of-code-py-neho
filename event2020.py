# -*- coding: utf-8 -*-

import re
import operator
from itertools import combinations
from functools import reduce
from collections import defaultdict


def day1_part1(in_: str) -> None:
    entries = [int(x) for x in in_.splitlines()]
    print(next(x * y for x, y in combinations(entries, 2) if x + y == 2020))


def day1_part2(in_: str) -> None:
    entries = [int(x) for x in in_.splitlines()]
    print(next(x * y * z for x, y, z in combinations(entries, 3) if x + y + z == 2020))


def day2_part1(in_: str) -> None:
    r_line = re.compile(r'(\d+)-(\d+) ([a-z]): ([a-z]+)')

    valids_count = 0
    for line in in_.splitlines():
        low_num, high_num, letter, password = r_line.match(line).groups()
        low_num, high_num = int(low_num), int(high_num)
        if low_num <= password.count(letter) <= high_num:
            valids_count += 1

    print(valids_count)


def day2_part2(in_: str) -> None:
    r_line = re.compile(r'(\d+)-(\d+) ([a-z]): ([a-z]+)')

    valids_count = 0
    for line in in_.splitlines():
        low_num, high_num, letter, password = r_line.match(line).groups()
        low_num, high_num = int(low_num), int(high_num)
        if (password[low_num - 1] == letter or password[high_num - 1] == letter) \
                and password[low_num - 1] != password[high_num - 1]:
            valids_count += 1

    print(valids_count)


def day3_part1(in_: str) -> None:
    pos_x, pos_y, trees_count = 3, 1, 0
    lines = in_.splitlines()
    lines_width = len(lines[0])
    while pos_y < len(lines):
        if lines[pos_y][pos_x % lines_width] == '#':
            trees_count += 1
        pos_x += 3
        pos_y += 1
    print(trees_count)


def day3_part2(in_: str) -> None:
    slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
    trees_counts = []

    for slope_x, slope_y in slopes:
        pos_x, pos_y, trees_count = slope_x, slope_y, 0
        lines = in_.splitlines()
        lines_width = len(lines[0])
        while pos_y < len(lines):
            if lines[pos_y][pos_x % lines_width] == '#':
                trees_count += 1
            pos_x += slope_x
            pos_y += slope_y
        trees_counts.append(trees_count)
    print(reduce(operator.mul, trees_counts, 1))


def day4_part1(in_: str) -> None:
    r_field = r'([a-z]{3}):(\S+)'

    valid_count = 0
    for passport in in_.split('\n\n'):
        fields = {k: v for k, v in re.findall(r_field, passport)}
        if 'byr' in fields.keys() and \
                'iyr' in fields.keys() and \
                'eyr' in fields.keys() and \
                'hgt' in fields.keys() and \
                'hcl' in fields.keys() and \
                'ecl' in fields.keys() and \
                'pid' in fields.keys():
            valid_count += 1
    print(valid_count)


def day4_part2(in_: str) -> None:
    r_field = r'([a-z]{3}):(\S+)'

    valid_count = 0
    for passport in in_.split('\n\n'):
        fields = {k: v for k, v in re.findall(r_field, passport)}

        if not (
                'byr' in fields.keys() and
                re.fullmatch(r'[0-9]{4}', fields['byr']) is not None and
                1920 <= int(fields['byr']) <= 2002
        ):
            continue

        if not (
                'iyr' in fields.keys() and
                re.fullmatch(r'[0-9]{4}', fields['iyr']) is not None and
                2010 <= int(fields['iyr']) <= 2020
        ):
            continue

        if not (
                'eyr' in fields.keys() and
                re.fullmatch(r'[0-9]{4}', fields['eyr']) is not None and
                2020 <= int(fields['eyr']) <= 2030
        ):
            continue

        if not (
                'hgt' in fields.keys() and (
                    (
                        re.fullmatch(r'[0-9]+cm', fields['hgt']) is not None and
                        150 <= int(fields['hgt'][:-2]) <= 193
                    ) or (
                        re.fullmatch(r'[0-9]+in', fields['hgt']) is not None and
                        59 <= int(fields['hgt'][:-2]) <= 76
                    )
                )
        ):
            continue

        if not (
                'hcl' in fields.keys() and
                re.fullmatch(r'#[0-9a-f]{6}', fields['hcl']) is not None
        ):
            continue

        if not (
                'ecl' in fields.keys() and
                fields['ecl'] in ('amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth')
        ):
            continue

        if not (
                'pid' in fields.keys() and
                re.fullmatch(r'[0-9]{9}', fields['pid']) is not None
        ):
            continue

        valid_count += 1
    print(valid_count)


def day5_part1(in_: str) -> None:
    max_id = 0;
    for passe in in_.splitlines():
        row_bounds = [0, 127]
        for c in passe[:7]:
            if c == 'F':
                row_bounds[1] = row_bounds[1] - ((row_bounds[1] - row_bounds[0]) // 2 + 1)
            else:
                row_bounds[0] = row_bounds[0] + ((row_bounds[1] - row_bounds[0]) // 2 + 1)

        col_bounds = [0, 7]
        for c in passe[7:]:
            if c == 'L':
                col_bounds[1] = col_bounds[1] - ((col_bounds[1] - col_bounds[0]) // 2 + 1)
            else:
                col_bounds[0] = col_bounds[0] + ((col_bounds[1] - col_bounds[0]) // 2 + 1)

        max_id = max(max_id, row_bounds[0] * 8 + col_bounds[0])
    print(max_id)


def day5_part2(in_: str) -> None:
    ids = set();
    for passe in in_.splitlines():
        row_bounds = [0, 127]
        for c in passe[:7]:
            if c == 'F':
                row_bounds[1] = row_bounds[1] - ((row_bounds[1] - row_bounds[0]) // 2 + 1)
            else:
                row_bounds[0] = row_bounds[0] + ((row_bounds[1] - row_bounds[0]) // 2 + 1)

        col_bounds = [0, 7]
        for c in passe[7:]:
            if c == 'L':
                col_bounds[1] = col_bounds[1] - ((col_bounds[1] - col_bounds[0]) // 2 + 1)
            else:
                col_bounds[0] = col_bounds[0] + ((col_bounds[1] - col_bounds[0]) // 2 + 1)

        ids.add(row_bounds[0] * 8 + col_bounds[0])

    for i in range(127 * 8 + 7):
        if i not in ids and i - 1 in ids and i + 1 in ids:
            print(i)


def day6_part1(in_: str) -> None:
    total = 0
    for group in in_.split('\n\n'):
        combined_answers = set()
        for person in group.splitlines():
            combined_answers = combined_answers.union(set(person))
        total += len(combined_answers)
    print(total)


def day6_part2(in_: str) -> None:
    total = 0
    for group in in_.split('\n\n'):
        persons = group.splitlines()
        combined_answers = set(persons[0])
        for person in persons[1:]:
            combined_answers.intersection_update(set(person))
        total += len(combined_answers)
    print(total)


def day7_part1(in_: str) -> None:
    r_rule = r'(.+?) bags contain (.+).'
    r_inner = r'(\d+) (.+?) bags?'

    rules = {}
    for outer, inners_str in [re.fullmatch(r_rule, line).groups() for line in in_.splitlines()]:
        inners = set()
        for inner in inners_str.split(', '):
            match = re.fullmatch(r_inner, inner)
            if match is None:
                continue
            inners.add(match.group(2))
        rules[outer] = inners

    valid_rules = {outer for outer, inners in rules.items() if 'shiny gold' in inners}
    prev_valid_rules = None
    while valid_rules != prev_valid_rules:
        prev_valid_rules = valid_rules.copy()
        for outer, inners in rules.items():
            for inner in inners:
                if inner in valid_rules:
                    valid_rules.add(outer)

    print(len(valid_rules))


def day7_part2(in_: str) -> None:
    r_rule = r'(.+?) bags contain (.+).'
    r_inner = r'(\d+) (.+?) bags?'

    rules = {}
    for outer, inners_str in [re.fullmatch(r_rule, line).groups() for line in in_.splitlines()]:
        inners = set()
        for inner in inners_str.split(', '):
            match = re.fullmatch(r_inner, inner)
            if match is None:
                continue
            inners.add((match.group(2), int(match.group(1))))
        rules[outer] = inners

    bags_count_per_bag = {outer: 0 for outer, inners in rules.items() if len(inners) == 0}
    while bags_count_per_bag.get('shiny gold') is None:
        for outer, inners in rules.items():
            if outer in bags_count_per_bag.keys():
                continue

            count = 0
            for inner, size in inners:
                if bags_count_per_bag.get(inner) is None:
                    break
                count += size * (1 + bags_count_per_bag[inner])
            else:
                bags_count_per_bag[outer] = count
    print(bags_count_per_bag['shiny gold'])

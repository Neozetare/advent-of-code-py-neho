# -*- coding: utf-8 -*-
import math
import re
import itertools
from collections import defaultdict, namedtuple
from functools import reduce
from operator import mul
from typing import List, Set, Tuple


def day1_part1(in_: str) -> None:
    nums = [int(x) for x in in_.splitlines()]
    print(sum(cur > prev for prev, cur in zip(nums, nums[1:])))


def day1_part2(in_: str) -> None:
    nums = [int(x) for x in in_.splitlines()]
    print(sum(b + c + d > a + b + c for a, b, c, d in zip(nums, nums[1:], nums[2:], nums[3:])))


def day2_part1(in_: str) -> None:
    horizontal_position = depth = 0
    for line in in_.splitlines():
        command, amount = line.split(' ')
        amount = int(amount)
        if command == 'forward':
            horizontal_position += amount
        else:
            depth += amount if command == 'down' else - amount
    print(horizontal_position * depth)


def day2_part2(in_: str) -> None:
    horizontal_position = depth = aim = 0
    for line in in_.splitlines():
        command, amount = line.split(' ')
        amount = int(amount)
        if command == 'forward':
            horizontal_position += amount
            depth += aim * amount
        else:
            aim += amount if command == 'down' else - amount
    print(horizontal_position * depth)


def day3_part1(in_: str) -> None:
    lines = in_.splitlines()
    report_width = len(lines[0])

    gamma_rate_str = ""
    for i in range(report_width):
        slice_ = [line[i] for line in lines]
        gamma_rate_str += max(slice_, key=slice_.count)
    gamma_rate = int(gamma_rate_str, 2)
    epsilon_rate = 2 ** report_width - gamma_rate - 1

    print(gamma_rate * epsilon_rate)


def day3_part2(in_: str) -> None:
    lines = in_.splitlines()
    report_width = len(lines[0])

    oxigen_generator_lines = lines.copy()
    for i in range(report_width):
        slice_ = [line[i] for line in oxigen_generator_lines]
        most_common = '1' if slice_.count('1') >= len(slice_) / 2 else '0'
        oxigen_generator_lines = [line for line in oxigen_generator_lines if line[i] == most_common]
        if len(oxigen_generator_lines) == 1:
            break

    co2_scrubber_lines = lines.copy()
    for i in range(report_width):
        slice_ = [line[i] for line in co2_scrubber_lines]
        most_common = '1' if slice_.count('1') >= len(slice_) / 2 else '0'
        co2_scrubber_lines = [line for line in co2_scrubber_lines if line[i] != most_common]
        if len(co2_scrubber_lines) == 1:
            break

    print(int(oxigen_generator_lines[0], 2) * int(co2_scrubber_lines[0], 2))


def day4_part1(in_: str) -> None:
    blocs = in_.split('\n\n')
    draw_list = [int(x) for x in blocs[0].split(',')]
    num_boards = [[[int(x) for x in row.split()] for row in board.splitlines()] for board in blocs[1:]]
    boards_length, board_height, board_width = len(num_boards), len(num_boards[0]), len(num_boards[0][0])
    mark_boards = [[[False for x in range(board_width)] for y in range(board_height)] for n in range(boards_length)]

    n = y = x = 0
    try:
        for draw in draw_list:
            for n, y, x in itertools.product(range(boards_length), range(board_height), range(board_width)):
                if num_boards[n][y][x] == draw:
                    mark_boards[n][y][x] = True
                    if all(mark_boards[n][y]) or all([row[x] for row in mark_boards[n]]):
                        raise StopIteration
    except StopIteration:
        pass

    won_number = num_boards[n][y][x]
    sum_unmarked = 0
    for y, x in itertools.product(range(board_height), range(board_width)):
        if not mark_boards[n][y][x]:
            sum_unmarked += num_boards[n][y][x]
    print(won_number * sum_unmarked)


def day4_part2(in_: str) -> None:
    blocs = in_.split('\n\n')
    draw_list = [int(x) for x in blocs[0].split(',')]
    num_boards = [[[int(x) for x in row.split()] for row in board.splitlines()] for board in blocs[1:]]
    boards_length, board_height, board_width = len(num_boards), len(num_boards[0]), len(num_boards[0][0])
    mark_boards = [[[False for x in range(board_width)] for y in range(board_height)] for n in range(boards_length)]

    n = y = x = 0
    ignored_boards = []
    try:
        for draw in draw_list:
            for n in range(boards_length):
                if n in ignored_boards:
                    continue
                for y, x in itertools.product(range(board_height), range(board_width)):
                    if num_boards[n][y][x] == draw:
                        mark_boards[n][y][x] = True
                        if all(mark_boards[n][y]) or all([row[x] for row in mark_boards[n]]):
                            if boards_length - len(ignored_boards) == 1:
                                raise StopIteration
                            ignored_boards.append(n)
                            break
    except StopIteration:
        pass

    won_number = num_boards[n][y][x]
    sum_unmarked = 0
    for y, x in itertools.product(range(board_height), range(board_width)):
        if not mark_boards[n][y][x]:
            sum_unmarked += num_boards[n][y][x]
    print(won_number * sum_unmarked)


def day5_part1(in_: str) -> None:
    r_line = r'(\d+),(\d+) -> (\d+),(\d+)'

    hydrothermal_points = defaultdict(lambda: 0)
    for x1, y1, x2, y2 in [[int(n) for n in re.match(r_line, line).groups()] for line in in_.splitlines()]:
        if not (x1 == x2 or y1 == y2):
            continue
        x_range = range(x1, x2 + 1) if x1 <= x2 else range(x1, x2 - 1, -1)
        y_range = range(y1, y2 + 1) if y1 <= y2 else range(y1, y2 - 1, -1)
        for x, y in itertools.product(x_range, y_range):
            hydrothermal_points[x, y] += 1

    print(len([v for v in hydrothermal_points.values() if v >= 2]))


def day5_part2(in_: str) -> None:
    r_line = r'(\d+),(\d+) -> (\d+),(\d+)'

    hydrothermal_points = defaultdict(lambda: 0)
    for x1, y1, x2, y2 in [[int(n) for n in re.match(r_line, line).groups()] for line in in_.splitlines()]:
        x_range = range(x1, x2 + 1) if x1 <= x2 else range(x1, x2 - 1, -1)
        y_range = range(y1, y2 + 1) if y1 <= y2 else range(y1, y2 - 1, -1)

        xy_range = itertools.product(x_range, y_range) if x1 == x2 or y1 == y2 else zip(x_range, y_range)
        for x, y in xy_range:
            hydrothermal_points[x, y] += 1

    print(len([v for v in hydrothermal_points.values() if v >= 2]))


def day6_part1(in_: str) -> None:
    lanternfish_count = [0] * 9
    for x in in_.split(','):
        lanternfish_count[int(x)] += 1

    for _ in range(80):
        lanternfish_count = lanternfish_count[1:9] + lanternfish_count[0:1]
        lanternfish_count[6] += lanternfish_count[8]
    print(sum(lanternfish_count))


def day6_part2(in_: str) -> None:
    lanternfish_count = [0] * 9
    for x in in_.split(','):
        lanternfish_count[int(x)] += 1

    for _ in range(256):
        lanternfish_count = lanternfish_count[1:9] + lanternfish_count[0:1]
        lanternfish_count[6] += lanternfish_count[8]
    print(sum(lanternfish_count))


def day7_part1(in_: str) -> None:
    positions = [int(x) for x in in_.split(',')]
    min_pos, max_pos = min(positions), max(positions)

    min_cost = math.inf
    for dest in range(min_pos, max_pos + 1):
        cost = sum([abs(pos - dest) for pos in positions])
        min_cost = min(cost, min_cost)
    print(min_cost)


def day7_part2(in_: str) -> None:
    positions = [int(x) for x in in_.split(',')]
    min_pos, max_pos = min(positions), max(positions)

    def cost_func(x):
        return x * (x + 1) // 2

    min_cost = math.inf
    for dest in range(min_pos, max_pos + 1):
        cost = sum([cost_func(abs(pos - dest)) for pos in positions])
        min_cost = min(cost, min_cost)
    print(min_cost)


def day8_part1(in_: str) -> None:
    four_digits = [line.split('|')[1].strip().split(' ') for line in in_.splitlines()]
    print(sum([len([digit for digit in four_digit if len(digit) in (2, 3, 4, 7)]) for four_digit in four_digits]))


def day8_part2(in_: str) -> None:
    signals_to_digit = {
        frozenset('abcefg'): 0,
        frozenset('cf'): 1,
        frozenset('acdeg'): 2,
        frozenset('acdfg'): 3,
        frozenset('bcdf'): 4,
        frozenset('abdfg'): 5,
        frozenset('abdefg'): 6,
        frozenset('acf'): 7,
        frozenset('abcdefg'): 8,
        frozenset('abcdfg'): 9,
    }

    total = 0
    for line in in_.splitlines():
        patterns, digits = [[set(word) for word in part.strip().split(' ')] for part in line.split('|')]

        signals_found = {sig: None for sig in 'abcdefg'}
        patterns_found = {n: None for n in range(10)}

        for pattern in patterns:
            size = len(pattern)
            if size == 2:
                patterns_found[1] = pattern
            elif size == 3:
                patterns_found[7] = pattern
            elif size == 4:
                patterns_found[4] = pattern
            elif size == 7:
                patterns_found[8] = pattern

        sigs_a = patterns_found[7] - patterns_found[1]
        signals_found[sigs_a.pop()] = 'a'

        sigs_bd = (patterns_found[4] - patterns_found[1])
        sigs_cf = patterns_found[1]
        for pattern in [p for p in patterns if len(p) == 6]:
            sigs_d = sigs_bd - pattern
            if len(sigs_d) == 1:
                sigs_b = sigs_bd - sigs_d
                signals_found[sigs_b.pop()] = 'b'
                signals_found[sigs_d.pop()] = 'd'
            sigs_c = sigs_cf - pattern
            if len(sigs_c) == 1:
                sigs_f = sigs_cf - sigs_c
                signals_found[sigs_c.pop()] = 'c'
                signals_found[sigs_f.pop()] = 'f'

        sigs_eg = set('abcdefg') - {k for k, v in signals_found.items() if v is not None}
        for pattern in patterns:
            sigs_e = sigs_eg - pattern
            if len(sigs_e) == 1:
                sigs_g = sigs_eg - sigs_e
                signals_found[sigs_e.pop()] = 'e'
                signals_found[sigs_g.pop()] = 'g'

        real_digits = [{signals_found[sig] for sig in digit} for digit in digits]
        real_digits_ints = [signals_to_digit[frozenset(digit)] for digit in real_digits]
        total += int(''.join(str(i) for i in real_digits_ints))
    print(total)


def day9_part1(in_: str) -> None:
    heightmap = [[int(c) for c in line] for line in in_.splitlines()]
    size_y, size_x = len(heightmap), len(heightmap[0])

    Point = namedtuple('Point', ['x', 'y'])

    def get_neighbors(point):
        neighbors = []
        if point.y > 0:
            neighbors.append(Point(point.x, point.y - 1))
        if point.y < size_y - 1:
            neighbors.append(Point(point.x, point.y + 1))
        if point.x > 0:
            neighbors.append(Point(point.x - 1, point.y))
        if point.x < size_x - 1:
            neighbors.append(Point(point.x + 1, point.y))
        return neighbors

    low_points = set()
    for y in range(size_y):
        for x in range(size_x):
            current = Point(x, y)
            if all(heightmap[current.y][current.x] < heightmap[n.y][n.x] for n in get_neighbors(current)):
                low_points.add(current)
    print(sum([heightmap[p.y][p.x] for p in low_points]) + len(low_points))


def day9_part2(in_: str) -> None:
    heightmap = [[int(c) for c in line] for line in in_.splitlines()]
    size_y, size_x = len(heightmap), len(heightmap[0])

    Point = namedtuple('Point', ['x', 'y'])

    def get_neighbors(point):
        neighbors = []
        if point.y > 0:
            neighbors.append(Point(point.x, point.y - 1))
        if point.y < size_y - 1:
            neighbors.append(Point(point.x, point.y + 1))
        if point.x > 0:
            neighbors.append(Point(point.x - 1, point.y))
        if point.x < size_x - 1:
            neighbors.append(Point(point.x + 1, point.y))
        return neighbors

    low_points = set()
    for y in range(size_y):
        for x in range(size_x):
            current = Point(x, y)
            if all(heightmap[current.y][current.x] < heightmap[n.y][n.x] for n in get_neighbors(current)):
                low_points.add(current)

    basins_sizes = []
    for low_point in low_points:
        to_explore = {low_point}
        explored = set()

        while len(to_explore) > 0:
            current = to_explore.pop()
            for n in get_neighbors(current):
                if heightmap[n.y][n.x] == 9:
                    continue
                if n in explored:
                    continue
                to_explore.add(n)
            explored.add(current)
        basins_sizes.append(len(explored))

    print(reduce(mul, sorted(basins_sizes, reverse=True)[:3]))


def day10_part1(in_: str) -> None:
    open_to_close = {'(': ')', '[': ']', '{': '}', '<': '>'}
    close_to_score = {')': 3, ']': 57, '}': 1197, '>': 25137}

    score = 0
    for line in in_.splitlines():
        opened_stack = []
        for c in line:
            if c in open_to_close.keys():
                opened_stack.append(c)
            elif c != open_to_close[opened_stack.pop()]:
                score += close_to_score[c]
                break
    print(score)


def day10_part2(in_: str) -> None:
    open_to_close = {'(': ')', '[': ']', '{': '}', '<': '>'}
    close_to_score = {')': 1, ']': 2, '}': 3, '>': 4}

    scores = []
    for line in in_.splitlines():
        opened_stack = []
        for c in line:
            if c in open_to_close.keys():
                opened_stack.append(c)
            elif c != open_to_close[opened_stack.pop()]:
                break
        else:
            scores.append(reduce(lambda s, o: s * 5 + close_to_score[open_to_close[o]], opened_stack[::-1], 0))
    print(sorted(scores)[len(scores) // 2])


def day11_part1(in_: str) -> None:
    #     in_ = """5483143223
    # 2745854711
    # 5264556173
    # 6141336146
    # 6357385478
    # 4167524645
    # 2176841721
    # 6882881134
    # 4846848554
    # 5283751526"""

    Point = namedtuple('Point', ['x', 'y'])

    class Octopus:
        def __init__(self, energy, position):
            self.energy = energy
            self.position = position
            self.last_flash = -1

        def neighbors(self, grid):
            if self.position.y > 0:
                yield grid[self.position.y - 1][self.position.x]
            if self.position.y < len(grid) - 1:
                yield grid[self.position.y + 1][self.position.x]
            if self.position.x > 0:
                yield grid[self.position.y][self.position.x - 1]
            if self.position.x < len(grid[self.position.y]) - 1:
                yield grid[self.position.y][self.position.x + 1]

            if self.position.y > 0 and self.position.x > 0:
                yield grid[self.position.y - 1][self.position.x - 1]
            if self.position.y > 0 and self.position.x < len(grid[self.position.y]) - 1:
                yield grid[self.position.y - 1][self.position.x + 1]
            if self.position.y < len(grid) - 1 and self.position.x > 0:
                yield grid[self.position.y + 1][self.position.x - 1]
            if self.position.y < len(grid) - 1 and self.position.x < len(grid[self.position.y]) - 1:
                yield grid[self.position.y + 1][self.position.x + 1]

    octopus_grid = [[Octopus(int(car), Point(x, y)) for x, car in enumerate(line)] for y, line in
                    enumerate(in_.splitlines())]

    num_flashes = 0
    for i in range(100):
        to_flash = set()
        flashed = set()

        for line in octopus_grid:
            for octopus in line:
                octopus.energy += 1
                if octopus.energy > 9:
                    to_flash.add(octopus)

        while len(to_flash) > 0:
            current = to_flash.pop()
            current.energy = 0
            num_flashes += 1
            flashed.add(current)
            for neighbor in current.neighbors(octopus_grid):
                if neighbor in flashed:
                    continue
                neighbor.energy += 1
                if neighbor.energy > 9:
                    to_flash.add(neighbor)
    print(num_flashes)


def day11_part2(in_: str) -> None:
    Point = namedtuple('Point', ['x', 'y'])

    class Octopus:
        def __init__(self, energy, position):
            self.energy = energy
            self.position = position
            self.last_flash = -1

        def neighbors(self, grid):
            if self.position.y > 0:
                yield grid[self.position.y - 1][self.position.x]
            if self.position.y < len(grid) - 1:
                yield grid[self.position.y + 1][self.position.x]
            if self.position.x > 0:
                yield grid[self.position.y][self.position.x - 1]
            if self.position.x < len(grid[self.position.y]) - 1:
                yield grid[self.position.y][self.position.x + 1]

            if self.position.y > 0 and self.position.x > 0:
                yield grid[self.position.y - 1][self.position.x - 1]
            if self.position.y > 0 and self.position.x < len(grid[self.position.y]) - 1:
                yield grid[self.position.y - 1][self.position.x + 1]
            if self.position.y < len(grid) - 1 and self.position.x > 0:
                yield grid[self.position.y + 1][self.position.x - 1]
            if self.position.y < len(grid) - 1 and self.position.x < len(grid[self.position.y]) - 1:
                yield grid[self.position.y + 1][self.position.x + 1]

    octopus_grid = [[Octopus(int(car), Point(x, y)) for x, car in enumerate(line)] for y, line in
                    enumerate(in_.splitlines())]

    for i in itertools.count(start=1, step=1):
        to_flash = set()
        flashed = set()

        for line in octopus_grid:
            for octopus in line:
                octopus.energy += 1
                if octopus.energy > 9:
                    to_flash.add(octopus)

        while len(to_flash) > 0:
            current = to_flash.pop()
            current.energy = 0
            flashed.add(current)
            for neighbor in current.neighbors(octopus_grid):
                if neighbor in flashed:
                    continue
                neighbor.energy += 1
                if neighbor.energy > 9:
                    to_flash.add(neighbor)

        if len(flashed) == len(octopus_grid) * len(octopus_grid[0]):
            print(i)
            break


def day12_part1(in_: str) -> None:
    connections = defaultdict(set)
    for line in in_.splitlines():
        connection = line.split('-')
        connections[connection[0]].add(connection[1])
        connections[connection[1]].add(connection[0])

    def paths_to_end(path: List[str]) -> Set[Tuple[str]]:
        paths = set()
        for connection in connections[path[-1]]:
            if connection == 'end':
                paths.add(tuple(path[:] + [connection]))
                continue
            if connection.islower() and connection in path:
                continue
            paths.update(paths_to_end(path[:] + [connection]))
        return paths

    print(len(paths_to_end(['start'])))


def day12_part2(in_: str) -> None:
    connections = defaultdict(set)
    for line in in_.splitlines():
        connection = line.split('-')
        connections[connection[0]].add(connection[1])
        connections[connection[1]].add(connection[0])

    def paths_to_end(path: List[str], bonus_lower_used=False) -> Set[Tuple[str]]:
        paths = set()
        for connection in connections[path[-1]]:
            if connection == 'end':
                paths.add(tuple(path[:] + [connection]))
                continue
            if connection.islower() and connection in path:
                if bonus_lower_used or connection in ('start', 'end'):
                    continue
                else:
                    paths.update(paths_to_end(path[:] + [connection], True))
                    continue
            paths.update(paths_to_end(path[:] + [connection], bonus_lower_used))
        return paths

    print(len(paths_to_end(['start'])))
